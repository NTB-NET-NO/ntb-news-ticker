
Imports System.net
Imports System.Text
Imports System.Globalization
Imports System.Security.Principal
Imports System.Security.Permissions
Imports System.Runtime.InteropServices
Imports System.Environment
Imports System.Security.Principal.WindowsIdentity







Public Class frmConfig
    Public UserName As String = My.Settings.RSSUserName
    Public PassWord As String = My.Settings.RSSPassWord
    Public RssPath As String = My.Settings.RSSServer

    Dim enUSCulture As New CultureInfo("en-US", True)

    Public TickerSpeed As Single = Val(My.Settings.TickerSpeed) * 10

    Public SavedConfig As Boolean
    Public ChangedConfig As Boolean


    Private Sub frmConfig_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtUsername.Text = UserName
        txtPassword.Text = PassWord
        txtURL.Text = RssPath
        SavedConfig = False
        ChangedConfig = False


        trackSpeed.Value = TickerSpeed

        ' Get the current identity.
        Dim MyIdent As WindowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent



        ' Create a principal.
        Dim MyPrincipal As New WindowsPrincipal(MyIdent)

        ' Check the role using a string.
        If MyPrincipal.IsInRole("BUILTIN\Administrators") Then
            grpInnstillinger.Visible = False
            grpSpeed.Top = grpInnstillinger.Top
        Else
            grpInnstillinger.Visible = True
            grpSpeed.Top = grpInnstillinger.Top + grpInnstillinger.Height + 10
        End If


    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        SavedConfig = True
        ChangedConfig = False
        My.Settings.Reload()
        Me.Close()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

        'UpdateAppSettings("RSSUserName", txtUsername.Text)
        'UpdateAppSettings("RSSPassWord", txtPassword.Text)
        'UpdateAppSettings("RSSServer", txtURL.Text)
        'UpdateAppSettings("TickerSpeed", trackSpeed.Value / 10)

        My.Settings.TickerSpeed = trackSpeed.Value / 10
        My.Settings.RSSPassWord = txtPassword.Text
        My.Settings.RSSServer = txtURL.Text
        My.Settings.RSSUserName = txtUsername.Text

        My.Settings.Save()
        SavedConfig = True
        ChangedConfig = True

        Me.Close()

    End Sub
End Class