<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConfig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtUsername = New System.Windows.Forms.TextBox
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.txtURL = New System.Windows.Forms.TextBox
        Me.trackSpeed = New System.Windows.Forms.TrackBar
        Me.grpSpeed = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.grpInnstillinger = New System.Windows.Forms.GroupBox
        Me.btnOk = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        CType(Me.trackSpeed, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpSpeed.SuspendLayout()
        Me.grpInnstillinger.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Brukernavn"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Passord"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(10, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "URL"
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(78, 19)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(204, 20)
        Me.txtUsername.TabIndex = 4
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(78, 45)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(204, 20)
        Me.txtPassword.TabIndex = 5
        '
        'txtURL
        '
        Me.txtURL.Location = New System.Drawing.Point(78, 71)
        Me.txtURL.Name = "txtURL"
        Me.txtURL.Size = New System.Drawing.Size(204, 20)
        Me.txtURL.TabIndex = 6
        '
        'trackSpeed
        '
        Me.trackSpeed.LargeChange = 1
        Me.trackSpeed.Location = New System.Drawing.Point(6, 43)
        Me.trackSpeed.Minimum = 2
        Me.trackSpeed.Name = "trackSpeed"
        Me.trackSpeed.Size = New System.Drawing.Size(276, 45)
        Me.trackSpeed.TabIndex = 8
        Me.trackSpeed.Value = 2
        '
        'grpSpeed
        '
        Me.grpSpeed.Controls.Add(Me.Label4)
        Me.grpSpeed.Controls.Add(Me.trackSpeed)
        Me.grpSpeed.Location = New System.Drawing.Point(12, 137)
        Me.grpSpeed.Name = "grpSpeed"
        Me.grpSpeed.Size = New System.Drawing.Size(288, 100)
        Me.grpSpeed.TabIndex = 9
        Me.grpSpeed.TabStop = False
        Me.grpSpeed.Text = "Hastighet"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Ticker hastighet"
        '
        'grpInnstillinger
        '
        Me.grpInnstillinger.Controls.Add(Me.txtUsername)
        Me.grpInnstillinger.Controls.Add(Me.Label1)
        Me.grpInnstillinger.Controls.Add(Me.txtURL)
        Me.grpInnstillinger.Controls.Add(Me.Label2)
        Me.grpInnstillinger.Controls.Add(Me.txtPassword)
        Me.grpInnstillinger.Controls.Add(Me.Label3)
        Me.grpInnstillinger.Location = New System.Drawing.Point(12, 17)
        Me.grpInnstillinger.Name = "grpInnstillinger"
        Me.grpInnstillinger.Size = New System.Drawing.Size(288, 103)
        Me.grpInnstillinger.TabIndex = 10
        Me.grpInnstillinger.TabStop = False
        Me.grpInnstillinger.Text = "Brukerinstillinger"
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(144, 303)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 11
        Me.btnOk.Text = "&Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(225, 303)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 12
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(312, 338)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.grpInnstillinger)
        Me.Controls.Add(Me.grpSpeed)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmConfig"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Oppsett"
        CType(Me.trackSpeed, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpSpeed.ResumeLayout(False)
        Me.grpSpeed.PerformLayout()
        Me.grpInnstillinger.ResumeLayout(False)
        Me.grpInnstillinger.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtURL As System.Windows.Forms.TextBox
    Friend WithEvents trackSpeed As System.Windows.Forms.TrackBar
    Friend WithEvents grpSpeed As System.Windows.Forms.GroupBox
    Friend WithEvents grpInnstillinger As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
