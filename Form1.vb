Imports System.Xml
Imports System.net
Imports System.Text
Imports System.Configuration
Imports System.Configuration.ConfigurationSettings
Imports System.Globalization
Imports System.io
' Imports System.Timers
' Imports ntb_FuncLib






Public Class frmNewsTicker

    Dim widthX As Single
    Dim heightY As Single
    Dim g As Graphics
    Dim xmlst As String      'string from the xml file
    Dim fo As Font
    Dim mFontSize As Integer = 16 ' Font size.
    Dim str As String
    Dim strwidth As SizeF    'gets the xml string's width and height
    Dim douX As Double      'stores the xmlstring's width     


    ' Henter variabler som skal brukes 
    Public UserName As String = My.Settings.RSSUserName
    Public PassWord As String = My.Settings.RSSPassWord
    Public RssPath As String = My.Settings.RSSServer
    Public RSSFile As String = My.Settings.RSSFileName


    Dim enUSCulture As New CultureInfo("en-US", True)




    ' Public TickerSpeed As Single = Convert.ToSingle(AppSettings("Speed"))
    Public TickerSpeed As Single = Val(My.Settings.TickerSpeed)


    ' Drag and move the ticker
    Private bFormDragging As Boolean = False
    Private oPointClicked As Point

    Private Sub Form1_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        ' Then we can move the form around
    End Sub


    Private Sub PictureBox2_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles PictureBox2.Paint
        'Debug.Write(vbCr & strwidth.Width & vbCr)
        'Debug.Write(String.Format("widthX = {0}", CStr(widthX) & vbCr))
        ' Debug.Write("Dette er str: " & str)
        SetStyle(ControlStyles.AllPaintingInWmPaint Or _
            ControlStyles.OptimizedDoubleBuffer Or _
            ControlStyles.UserPaint, True)

        e.Graphics.Clear(Me.PictureBox2.BackColor)
        e.Graphics.DrawString(str, fo, Brushes.Blue, widthX, heightY)
        e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
        ' e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        e.Graphics.SmoothingMode = Drawing2D.SmoothingMode.HighSpeed




        

        ' Debug.Write(String.Format("the string width is {0}", CStr(strwidth.Width)))
        If widthX <= (0 - douX) Then
            widthX = Me.PictureBox1.Width

            ' Vi laster inn nyheter p� nytt slik at vi f�r de siste....
            Me.loadthenews()
        Else
            widthX -= TickerSpeed
        End If

        e.Dispose()

    End Sub
    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        init()


    End Sub

    Private Sub init()
        Me.Width = Screen.PrimaryScreen.WorkingArea.Width
        Me.Location = New Point(0, 100)
        Me.MaximumSize = New Point(Screen.PrimaryScreen.WorkingArea.Width, 50)
        Me.Height = 27
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Text = ""
        Me.Icon = Nothing

        ' Plasserer ticker nederst p� skjermen. 
        Me.Top = Screen.PrimaryScreen.WorkingArea.Height
        ' Me.TransparencyKey = Me.BackColor
        '        Me.Opacity = 0.5

        PictureBox2.Width = Me.Width - ToolStrip1.Width - PictureBox1.Width
        g = Me.PictureBox2.CreateGraphics 'get the graphics object of the form

        widthX = Me.PictureBox2.Width        ' x co- ordinate or width
        heightY = Me.PictureBox2.Height / 2 - mFontSize
        ' Debug.Write(String.Format("The width is {0}", �CStr(Me.Width)))


        Me.loadthenews()
        ' Dim gr As Graphics
        Timer1.Interval = 1
        Timer1.Start()

        fo = New Font("ARIAL", mFontSize, FontStyle.Bold, GraphicsUnit.Point)
        strwidth = g.MeasureString(str, fo)
        douX = strwidth.Width
    End Sub

    Private Sub loadthenews()
        ' Create an empty instance of the NetworkCredential class.
        ' Dim rssFeedPath As String = "\\bhutan\d$\RSS\1881\RSS\RSSFeed.xml"
        ' Create an empty instance of the NetworkCredential class.
        ' Dim rssFeedPath As String = "\\bhutan\d$\RSS\1881\RSS\RSSFeed.xml"

        Dim resolver As XmlUrlResolver = New XmlUrlResolver()
        Dim credentials As NetworkCredential = New NetworkCredential(UserName, PassWord)

        resolver.Credentials = credentials

        Dim docXML As New XmlDocument
        docXML.XmlResolver = resolver

        ' Getting the Document

        Try
            docXML.Load(My.Settings.RSSServer)
        Catch ex As Exception
            Debug.Write(ex.ToString)

        End Try

        Dim Nodes As XmlNodeList


        Nodes = docXML.SelectNodes("//channel/item/description")
        Dim title As String = docXML.SelectSingleNode("//channel/item/description").InnerText
        '        str = " * " & title
        If Nodes.Count <> 0 Then
            Dim x = Nodes.Count
            Dim y
            For y = 0 To x - 1
                str += " * " & Nodes.Item(y).InnerText.ToString()

            Next
        End If





    End Sub

    Private Sub form1_hover(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.MouseHover
        Timer1.Stop()


    End Sub

    Private Sub form1_leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.MouseLeave
        Timer1.Start()

    End Sub



    Private Sub tickerTypeWriter()
        ' Denne brukes ikke i koden, men jeg tar vare p� den for kanskje � bruke den et annet sted...
        Dim txtTicker As String = "NTB er verdens beste sted � jobbe!"
        Dim a

        Dim lblTicker As New Label
        Me.Controls.Add(lblTicker)



        For a = 1 To txtTicker.Length
            lblTicker.Text = txtTicker.Substring(0, a) & "_"
            System.Threading.Thread.Sleep(100)
            lblTicker.Update()


        Next
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.PictureBox2.Refresh()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbClose.Click
        Me.Close()
    End Sub

    Private Sub frmNewsTicker_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseUp, PictureBox2.MouseUp
        Me.bFormDragging = False
    End Sub

    Private Sub frmNewsTicker_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseMove, PictureBox2.MouseMove
        If Me.bFormDragging Then
            Dim oMoveToPoint As Point

            ' Use the current mouse position to find 
            ' the target location
            oMoveToPoint = Me.PointToScreen(New Point(e.X, e.Y))
            ' Adjust the position based on where you started
            oMoveToPoint.Offset(Me.oPointClicked.X * -1, _
               (Me.oPointClicked.Y + _
               SystemInformation.CaptionHeight + _
               SystemInformation.BorderSize.Height) * -1)

            ' Move the form
            Me.Location = oMoveToPoint
        End If
    End Sub

    Private Sub frmNewsTicker_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MyBase.MouseDown, PictureBox2.MouseDown
        ' Skal vi pr�ve � dra den litt da?

        Me.bFormDragging = True
        Me.oPointClicked = New Point(e.X, e.Y)
    End Sub

    Private Sub tsbConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbConfig.Click
        Timer1.Stop()
        frmConfig.ShowDialog()


        If frmConfig.ChangedConfig = True Then
            Me.init()
            TickerSpeed = My.Settings.TickerSpeed
            fo = New Font("ARIAL", mFontSize, FontStyle.Bold, GraphicsUnit.Point)
            strwidth = g.MeasureString(str, fo)
            douX = strwidth.Width
            Timer1.Start()
        Else
            Timer1.Start()

        End If
    End Sub

    Private Sub tsbRestart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tsbRestart.Click
        ' Denne skal restarte "skiten"

        Try
            Timer1.Stop()
            str = Nothing
            Me.init()
            Timer1.Start()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try


    End Sub
End Class
